package weatherservice.node.impl;

import java.lang.management.ManagementFactory;
import java.lang.management.OperatingSystemMXBean;

import org.osgi.framework.BundleContext;

import weatherservice.node.Node;

public class NodeImpl implements Node {
	private volatile BundleContext m_context;

	@Override
	public String name() {
		return "Node at port " + m_context.getProperty("org.osgi.service.http.port");
	}

	@Override
	public boolean healthCheck() {
		OperatingSystemMXBean osBean = ManagementFactory.getPlatformMXBean(OperatingSystemMXBean.class);
		double loadAverage = osBean.getSystemLoadAverage();
		return loadAverage < 3.0;
	}
}
