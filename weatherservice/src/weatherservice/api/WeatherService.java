package weatherservice.api;

public interface WeatherService {
	double predictTemperature(String city);
}
