package weatherservice.impl;

import weatherservice.api.WeatherService;

public class WeatherServiceImpl implements WeatherService {
	@Override
	public synchronized double predictTemperature(String city) {
		System.out.print("Calculating temperature for " + city + ": ");
		try {
			Thread.sleep(Long.valueOf(System.getProperty("delay", "2000")));
		}
		catch (InterruptedException e) {}
		int result = 20 + city.length();
		System.out.println(result);
		return result;
	}
}
