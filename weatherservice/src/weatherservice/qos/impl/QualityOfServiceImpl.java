package weatherservice.qos.impl;

import org.osgi.framework.ServiceRegistration;

import weatherservice.api.WeatherService;
import weatherservice.qos.QualityOfService;

public class QualityOfServiceImpl implements WeatherService, QualityOfService {
	private volatile ServiceRegistration m_registration;
	private volatile WeatherService m_service;
	private long m_invocations;
	private long m_totalTime;

	@Override
	public double predictTemperature(String city) {
		long t1 = System.currentTimeMillis();
		double temperature = m_service.predictTemperature(city);
		long t2 = System.currentTimeMillis();
		synchronized (this) {
			m_invocations++;
			m_totalTime += (t2 - t1);
		}
		return temperature;
	}

	@Override
	public long serviceId() {
		return (Long) m_registration.getReference().getProperty("org.apache.felix.dependencymanager.aspect");
	}
	
	@Override
	public String serviceName() {
		return "WeatherService";
	}
	
	@Override
	public synchronized long averageInvocationTime() {
		if (m_invocations == 0) {
			return 0;
		}
		return m_totalTime / m_invocations;
	}
}
