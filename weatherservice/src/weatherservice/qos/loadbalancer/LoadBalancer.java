package weatherservice.qos.loadbalancer;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

import weatherservice.api.WeatherService;
import weatherservice.qos.QualityOfService;

public class LoadBalancer implements WeatherService {
	private CopyOnWriteArrayList<QualityOfService> m_qualities = new CopyOnWriteArrayList<>();
	private volatile BundleContext m_context;

	public void add(QualityOfService service) {
		m_qualities.add(service);
	}

	public void remove(QualityOfService service) {
		m_qualities.remove(service);
	}

	@Override
	public double predictTemperature(String city) {
		CopyOnWriteArrayList<QualityOfService> qualities = m_qualities;
		double random = Math.random() * sum(qualities);
		for (QualityOfService qos : qualities) {
			double weight = weight(qos);
			if (random < weight) {
				WeatherService service = lookup(qos.serviceId());
				return service.predictTemperature(city);
			}
			else {
				random -= weight;
			}
		}
		return 0;
	}
	
	double sum(List<QualityOfService> list) {
		double sum = 0;
		for (QualityOfService qos : list) {
			sum += weight(qos);
		}
		return sum;
	}
	
	double weight(QualityOfService qos) {
		long time = qos.averageInvocationTime();
		return time == 0 ? 1.0 : 1.0/time;
	}
	
	WeatherService lookup(long id) {
		ServiceReference[] sr;
		try {
			sr = m_context.getServiceReferences(WeatherService.class.getName(), "(|(service.id=" + id + ")(org.apache.felix.dependencymanager.aspect=" + id + "))");
			if (sr != null && sr.length > 0) {
				Arrays.sort(sr);
				WeatherService service = (WeatherService) m_context.getService(sr[sr.length - 1]);
				return service;
			}
		}
		catch (InvalidSyntaxException e) {}
		return null;
	}
}
